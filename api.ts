import { BASE_URL } from 'react-native-dotenv'
import axiosDefault from 'axios';

const api_base = "/api/v1"

// set the axios base URL
let api = axiosDefault.create({
    // @ts-ignore
    baseURL: BASE_URL,

});
// set all the default headers
api.defaults.headers.common['Content-Type'] = 'application/json'

type User = {
    username: string,
    password?: string,
    token?: string,
    email: string,
}

type LoginDeets = {
    username: string,
    password: string
}

export function setApiToken(token: string) {
    // set the token for our api function 
    api.defaults.headers.common['Authorization'] = 'Token ' + token
}

export function createUser (data:User) {
    return api.post(api_base+'/users/', data)
}

export function login (data : LoginDeets) {
    return axiosDefault.post(BASE_URL+'/api-auth/', `username=${data.username}&password=${data.password}`)
}

export function uploadSymptoms (data) {
    return api.post(api_base+'/diagnosis/', data)
}

