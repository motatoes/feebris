import React, { Component } from 'react';
import {
    StyleSheet,
    AppRegistry, 
    Image,
    Text,
    Button,
    View,
    ImageSourcePropType
} from 'react-native';
import { uploadSymptoms } from "../api";
import { store } from '../store';
import { setResult } from '../store/actions'

type MyProps = {
    pic?: ImageSourcePropType,
    navigation: any
}

class Upload extends Component <MyProps, {}> {

    uploadSymptoms () {
        const {navigate} = this.props.navigation;
        console.log(store.getState().assessment)
        let promise = uploadSymptoms(store.getState().assessment)
        promise.then(res => {
            alert("results uploaded successfully, computing result ...");
            // update the store with the current result
            store.dispatch(setResult(res.data))
            navigate('Result');
        })

        promise.catch(err => {
            alert("results could not be uploaded, please try again");
            console.log(err.message)
            console.log(err.data)
        })
    }

    render () {
        const pic = require('../assets/complete.png')
        
        return (
        <View style={this.styles.container}>
            <View style={{height: 100}}>
                <Image source={pic} 
                style={{ width: undefined, height: 120 }}></Image>
            </View>
            <Text style={{fontSize: 40}}>Your results are ready to be uploaded for diagnosis</Text>
            <View style={{height: 50}}>
                <Button
                    title="Upload Symptoms"
                    onPress={() => this.uploadSymptoms()}
                />
            </View>
        </View>
    )}

    styles = StyleSheet.create({
        container: {
          flex: 1,
          backgroundColor: '#eee',
          flexDirection: 'column',
          alignItems: 'stretch',
          justifyContent: 'space-around',
          padding: 20
        },
      });
}

export default Upload

AppRegistry.registerComponent('Upload', () => Upload);
