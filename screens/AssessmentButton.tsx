import React, {
    Component,
} from 'react';

import {
    View,
    AppRegistry
} from 'react-native';

import {Button} from 'react-native-elements';
import {connect} from 'react-redux';
import { 
    store,
    UserType,
    StateType,
} from "../store";
import {logout} from '../store/actions';

type MyProps = {
    navigation?: any
}

const mapStateToProps = (state : StateType) => {
    return {
        user: state.user
        // assessment: state.assessment
    }
}


class AssessmentButton extends Component<MyProps, {}> {

    logout () {
        alert('logging out')
        store.dispatch(logout())
        console.log(store.getState())
    }

    render () {
        const {navigate} = this.props.navigation;
        // @ts-ignore
        if (store.getState().user.isAuthenticated) {
            return (
                <View>
                    <Button
                    title="Start Assessment"
                    onPress={() => navigate('Fever')}
                    />
                    <Button
                    style={{marginTop: 10}}
                    title="logout"
                    onPress={() => this.logout()}
                    />
                </View>
            )
        } else {
            return (
                <View>
                    <Button 
                    title="Login"
                    onPress={() => navigate('Login')}
                    />

                    <Button 
                    style={{marginTop: 10}}
                    title="Create User"
                    onPress={() => navigate('Register')}
                    />
                </View>
            )
        }
    }
}

export default connect(mapStateToProps)(AssessmentButton);
// export default (AssessmentButton);

AppRegistry.registerComponent('AssessmentButton', () => AssessmentButton);

