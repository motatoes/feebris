import React, { Component } from 'react';
import { 
    AppRegistry,
    StyleSheet,
    Text,
    TextInput,
    View,
    Image,
    ImageSourcePropType
} from 'react-native';

import {Button} from 'react-native-elements'
import {connect} from 'react-redux';
import {setTemprature} from '../store/actions';

type MyState = {
    text: string
}

type MyProps = {
    navigation: any,
    setTemprature: any,
    assessment: any,
}

class Thermometer extends Component <MyProps, MyState> {

    state = {
        text: "37"
    }
    
    render () {
        // this.props.setTemprature(37);
        const {navigate} = this.props.navigation;
        const pic = require('../assets/thermometer.png')

        return (
        <View style={this.styles.container}>
            {/* <Text>Welcome to Feebris!</Text> */}
            {/* <Home></Home> */}
            {/* <Blinker text="blinkit now"></Blinker> */}
            {/* experimenting with flex below */}
            <View style={{height: 100}}>
                <Image source={pic}
                style={{ width: 100, height: 100 }}></Image>
            </View>

            <Text style={{fontSize: 20}}>Measure your temperature using a thermometer</Text>

            <View style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}} >
                <TextInput
                    style={{fontSize: 40, height: 40}}
                    placeholder="Insert your temprature here"
                    onChangeText={(text)=>this.props.setTemprature(text)}
                    value={this.props.assessment.temperature.toString()}
                />
                <Text style={{fontSize: 20}} >degreese celcius</Text>
            </View>

            <View style={{height: 50}}>
                <Button
                    title="Next"
                    onPress={() => navigate('Cough')}
                />
            </View>
        </View>
    )}

    styles = StyleSheet.create({
        container: {
          flex: 1,
          backgroundColor: '#eee',
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'space-around',
          padding: 20
        },
      });
}


const mapStateToProps = (state) => {
    return {
        user: state.user,
        assessment: state.assessment
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setTemprature: (value) => {
            dispatch(setTemprature(value))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Thermometer);


AppRegistry.registerComponent('Thermometer', () => Thermometer);

