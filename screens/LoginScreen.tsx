import React, {
    Component
} from 'react';

import {
    AppRegistry,
    View,
    Text,
    Image,
} from 'react-native';

import { login } from '../api';

import { Button, Input } from 'react-native-elements';
import { store } from '../store';
import {setUser, setToken} from '../store/actions'
import {setApiToken} from '../api'

type MyProps = {
    navigation: any,
}

export default class LoginScreen extends Component<MyProps, {}> {

    login (state, navigate) {
        let promise = login({
            username: state.username,
            password: state.password
        })
        promise.then((res) => {
            store.dispatch(setToken(res.data.token))
            setApiToken(res.data.token)
            navigate('Home')
        })
        
        promise.catch((err) => {
            alert('Login failed')
        })
    }

    render () {
        const pic = require('../assets/login.png');
        const {navigate} = this.props.navigation;

        return (
            <View 
                style={{
                    flex: 1,
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: '#eee',
                    padding: 15
                }}>
                <Image 
                    source={pic}
                    style={{height: 100, width: 100}}
                />
                <Text style={{fontSize: 34}}>Enter your login details</Text>
                <View style={{width: '100%'}}>
                    <Input
                        placeholder='Your Username'
                        autoCapitalize = 'none'
                        onChangeText = {(text) => this.setState({username: text})}
                    />
                    <Input 
                        placeholder="Your password" 
                        textContentType="password" 
                        secureTextEntry={true}
                        style={{height: 50}}
                        onChangeText = {(text) => this.setState({password: text})}
                    />
                    <Button title="Login" 
                            onPress={() => this.login(this.state, navigate)}
                    ></Button>
                </View>
            </View>
        )
    }
}

AppRegistry.registerComponent('Login', () => LoginScreen);