import React, { Component } from 'react';
import { 
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Button,
    Image,
    ImageSourcePropType
} from 'react-native';

import {ButtonGroup} from 'react-native-elements';
import {connect} from 'react-redux';
import {setCough} from '../store/actions';
import { store } from '../store';


type MyProps = {
    pic?: ImageSourcePropType,
    navigation: any,
    setCough: any,
    user: any,
    assessment: any,
}

class Cough extends Component <MyProps, {}> {

    state = {
        selectedIndex: 2
    }

    updateIndex (selectedIndex) {
        if (selectedIndex == 0) {
            this.props.setCough(true);
        }
        else {
            this.props.setCough(false);
        }
        this.setState(() => {
            return {selectedIndex}
        })
        console.log(store.getState())
    }
      
    render () {
        const {navigate} = this.props.navigation;
        const pic = require('../assets/cough.png');
        const buttons = ['Yes', 'No'];
        const { selectedIndex } = this.state;

        return (
        <View style={this.styles.container}>
            {/* <Text>Welcome to Feebris!</Text> */}
            {/* <Home></Home> */}
            {/* <Blinker text="blinkit now"></Blinker> */}
            {/* experimenting with flex below */}
            <View style={{height: 100}}>
                <Image source={pic} 
                style={{ width: 100, height: 100 }}></Image>
            </View>
            <Text style={{fontSize: 20}}>Have you ever had a cough?</Text>

            <ButtonGroup
                onPress={this.updateIndex.bind(this)}
                selectedIndex={selectedIndex}
                buttons={buttons}
                containerStyle={{height: 100}}
            />

            <Button
                title="proceed"
                onPress={() => navigate('Upload')} />

        </View>
    )}

    styles = StyleSheet.create({
        container: {
          flex: 1,
          backgroundColor: '#eee',
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'space-around',
          padding: 20
        },
      });
}


const mapStateToProps = (state) => {
    return {
        user: state.user,
        assessment: state.assessment
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setCough: (value) => {
            dispatch(setCough(value))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Cough);

AppRegistry.registerComponent('Cough', () => Cough);

