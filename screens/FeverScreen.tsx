import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Button,
    Image,
    ImageSourcePropType
} from 'react-native';

import {ButtonGroup} from 'react-native-elements';
import {connect} from 'react-redux';
import {setFever} from '../store/actions'
import { store } from '../store';

type MyProps = {
    pic?: ImageSourcePropType,
    navigation: any,
    setFever: any,
}

class Fever extends Component <MyProps, {}> {

    state = {
        selectedIndex: 2
    }

    updateIndex (selectedIndex) {
        if (selectedIndex == 0) {
            this.props.setFever(true)
        }
        else {
            this.props.setFever(false)
        }
        this.setState(() => {
            return {selectedIndex}
        })
    }
      
    render () {
        const {navigate} = this.props.navigation;
        const pic = require('../assets/fever.png');
        const buttons = ['Yes', 'No'];
        const { selectedIndex } = this.state;

        return (
        <View style={this.styles.container}>
            {/* <Text>Welcome to Feebris!</Text> */}
            {/* <Home></Home> */}
            {/* <Blinker text="blinkit now"></Blinker> */}
            {/* experimenting with flex below */}
            <View style={{height: 100}}>
                <Image source={pic} 
                style={{ width: 100, height: 100 }}></Image>
            </View>
            <Text style={{fontSize: 20}}>Have you had a Fever over the past 5 days?</Text>

            <ButtonGroup
                onPress={this.updateIndex.bind(this)}
                selectedIndex={selectedIndex}
                buttons={buttons}
                containerStyle={{height: 100}}
            />

            <Button
                title="proceed"
                onPress={() => navigate('Thermometer')} />

        </View>
    )}

    styles = StyleSheet.create({
        container: {
          flex: 1,
          backgroundColor: '#eee',
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'space-around',
          padding: 20
        },
      });
}

const mapStateToProps = (state) => {
    return {
        user: state.user
        // assessment: state.assessment
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setFever: (value) => {
            dispatch(setFever(value))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Fever);

AppRegistry.registerComponent('Fever', () => Fever);

