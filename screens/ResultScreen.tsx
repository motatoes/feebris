import React, { Component } from 'react';
import {
    StyleSheet,
    AppRegistry, 
    Image,
    Text,
    Button,
    View,
    ActivityIndicator,
    ImageSourcePropType
} from 'react-native';

import {store} from '../store'

type MyState = {
    loading: any
}

type MyProps = {
    pic?: ImageSourcePropType,
    navigation: any
}

export default class Home extends Component <MyProps, MyState> {

    state = {
        loading: false
    }

    DiagnosisResult () {
        console.log(store.getState().result)
        if (store.getState().result.status === 'Pending') {
            return (
            <View style={{height: 100}}>
                <ActivityIndicator size="large" color="#0000ff" />
                <Text style={{fontSize: 40}}>Diagnosing Results ...</Text>
            </View>
            )
        }
        else if (store.getState().result.result === 'Flu') {
            return (
            <View style={{flex: 0, alignItems: 'center'}}>
                <Image style={{width: 100, height: 100}} source={require('../assets/cough.png')}></Image>
                <Text style={{fontSize: 40}}>Seems like you have a flu! please see a doctor</Text>
            </View>
            )
        }
        else if (store.getState().result.result === 'No_Flu') {
            return (
            <View style={{flex: 0, alignItems: 'center'}}>
                <Image style={{width: 100, height: 100}} source={require('../assets/healthy.png')}></Image>
                <Text style={{fontSize: 40}}>Seems like you are not sick!</Text>
            </View>
            )
        }
        else {
            return (<View></View>)
        }
    }
    render () {
        const {navigate} = this.props.navigation;
        const pic = require('../assets/complete.png')
        
        return (
            <View style={this.styles.container}>
                <this.DiagnosisResult></this.DiagnosisResult>
                <View style={{height: 50}}>
                    <Button
                        title="Start again"
                        onPress={() => navigate('Home')}
                    />
                </View>
            </View>
        )}

    styles = StyleSheet.create({
        container: {
          flex: 1,
          backgroundColor: '#eee',
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'space-around',
          padding: 20
        },
      });
}

AppRegistry.registerComponent('Home', () => Home);
