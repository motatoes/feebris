import React, {
    Component
} from 'react';

import {
    AppRegistry,
    View,
    Text,
    Image,
} from 'react-native';

import { createUser } from '../api';

import { Button, Input } from 'react-native-elements';
import { store } from '../store';
import {setUser, setToken} from '../store/actions'

type MyProps = {
    navigation: any,
}

export default class LoginScreen extends Component<MyProps, {}> {

    register (state, navigate) {
        let promise = createUser({
            username: state.username,
            password: state.password,
            email: state.email,
        })

        promise.then((res) => {
            alert("user created successfully, please login to continue")
            navigate('Home')
        })

        promise.catch((err) => {
            alert('User creation failed, please try again later')
        })

    }

    render () {
        const pic = require('../assets/register.png');
        const {navigate} = this.props.navigation;

        return (
            <View 
                style={{
                    flex: 1,
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: '#eee',
                    padding: 15
                }}>
                <Image 
                    source={pic}
                    style={{height: 100, width: 100}}
                />
                <Text style={{fontSize: 34}}>Enter your details</Text>
                <View style={{width: '100%'}}>
                <Input
                        placeholder='Your Username'
                        autoCapitalize = 'none'
                        onChangeText = {(text) => this.setState({username: text})}
                    />
                    <Input
                        placeholder='Your Email'
                        autoCapitalize = 'none'
                        onChangeText = {(text) => this.setState({email: text})}
                    />
                    <Input 
                        placeholder="Your password" 
                        textContentType="password" 
                        secureTextEntry={true}
                        style={{height: 50}}
                        onChangeText = {(text) => this.setState({password: text})}
                    />
                    <Button title="Login" 
                            onPress={() => this.register(this.state, navigate)}
                    ></Button>
                </View>
            </View>
        )
    }
}

AppRegistry.registerComponent('Login', () => LoginScreen);