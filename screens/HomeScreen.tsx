import React, { Component, useReducer } from 'react';
import {
    StyleSheet,
    AppRegistry,
    Image,
    Text,
    View,
    ImageSourcePropType
} from 'react-native';

import { withNavigationFocus } from 'react-navigation';

import {Button} from 'react-native-elements';

import {connect} from 'react-redux';
import {logout} from '../store/actions';
import {
    UserType,
    StateType,
    store,
} from '../store/index';
import AssessmentButton from './AssessmentButton';

type MyProps = {
    pic?: ImageSourcePropType,
    user: UserType,
    navigation: any,
    navigate: any,
    isFocused: boolean,
}

const mapStateToProps = (state : StateType) => {
    return {
        user: state.user,
        assessment: state.assessment
    }
}

const mapDispatchToProps = dispatch => {
    return {
    }
}

export default class HomeScreen extends Component <MyProps, {user: any}> {

    componentDidUpdate(prevProps) {
    console.log('component did update')
        if (this.props.isFocused && !prevProps.isFocused) {
          // Screen has now come into focus, perform your tasks here! 
            console.log('constructing home')
            console.log(store.getState())
        }
    }

    render () {
        const {navigate} = this.props.navigation;
        const pic = require('../assets/feebris.png')
        return (
            <View style={this.styles.container}>
                {/* <Text>Welcome to Feebris!</Text> */}
                {/* <Home></Home> */}
                {/* <Blinker text="blinkit now"></Blinker> */}
                {/* experimenting with flex below */}
                <View style={{height: 100}}>
                    <Image source={pic} 
                    style={{ width: undefined, height: 120 }}></Image>
                </View>
                <View style={{height: 50}}>
                    <AssessmentButton 
                        navigation={this.props.navigation}
                    />
                </View>
            </View>
        );
    }

    styles = StyleSheet.create({
        container: {
          flex: 1,
          backgroundColor: '#eee',
          flexDirection: 'column',
          alignItems: 'stretch',
          justifyContent: 'space-around',
          padding: 20
        },
      });
}

// export default connect(mapStateToProps)(withNavigationFocus(HomeScreen))


AppRegistry.registerComponent('Home', () => HomeScreen);
