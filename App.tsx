import React from 'react';
import {Provider} from 'react-redux';
import { ThemeProvider } from 'react-native-elements';
import { StyleSheet, Text, View } from 'react-native';
import {createStackNavigator, createAppContainer} from 'react-navigation';
import {store} from './store/index';
import HomeScreen from './screens/HomeScreen';
import LoginScreen from './screens/LoginScreen';
import RegisterScreen from './screens/RegisterScreen';
import FeverScreen from './screens/FeverScreen';
import ThermometerScreen from './screens/ThermometerScreen';
import CoughScreen from './screens/CoughScreen'
import UploadScreen from './screens/UploadScreen'
import ResultScreen from './screens/ResultScreen'

const MainNavigator = createStackNavigator({
  Home: {screen: HomeScreen},
  Login: {screen: LoginScreen},
  Register: {screen: RegisterScreen},
  Fever: {screen: FeverScreen},
  Thermometer: {screen: ThermometerScreen},
  Cough: {screen: CoughScreen},
  Upload: {screen: UploadScreen},
  Result: {screen: ResultScreen}
});

let Navigation = createAppContainer(MainNavigator);

class App extends React.Component {
  render () {
    return (
      <Provider store={store}>
        <ThemeProvider>
          <Navigation />
        </ThemeProvider>
      </Provider>
    );
  }
}

export default App;

// export default function App() {
//   return (
//     <View style={styles.container}>
//       {/* <Text>Welcome to Feebris!</Text> */}
//       {/* <Banana></Banana> */}
//       {/* <Blinker text="blinkit now"></Blinker> */}
//       <Text style={{textAlign: "center", fontSize: 24, marginTop: 40}}>Welcome to flex at Feebris</Text>
//       {/* experimenting with flex below */}
//       <View style={{flex: 1, backgroundColor: 'black'}}></View>
//       <View style={{flex: 2, backgroundColor: 'red'}}></View>
//       <View style={{flex: 3, backgroundColor: 'blue'}}></View>
//     </View>
//   );
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#eee',
//     flexDirection: 'column',
//     // alignItems: 'center',
//     justifyContent: 'space-between',
//     padding: 15
//   },
// });
