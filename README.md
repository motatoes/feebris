Feebris App!
======

![](static/feebris.gif)

This is the react-native component of the feebris app. The backend is on: https://gitlab.com/motatoes/feebris-backend.

The backend is a django app which exposes a few REST endpoints that are consumed by the native app:

```
/auth-token/ - used to login and obtain a token which should be sent by future requests
/api/v/1/users/ - user operations
/api/v1/diagnosis  - diagnosis and symptoms
```

We use Redux to maintain the app state.

Setting up for local development
=======
- clone this repository

```
git clone https://gitlab.com/motatoes/feebris
```

- clone the backend repository

```
git clone https://gitlab.com/motatoes/feebris-backend
```

- Run the backend server api after installing all requirements

```
pip install pipenv
pipenv shell
pipenv install
cd feebris
python manage.py runserver localhost:8000
# Note that the server should be running on port 8000 as the app proxies to this port
# if you wish to run the server on another port you need to modify the .env file from the other app
# to match this port 
```

- Finally move to the feebris app and start the app, you can do it in the expo simulator if you have npm v10.x

```
cd feebris/rn
npm install
npm start
# after it starts you can press i to launch the app in ios simulator
```

Future Scaling
======

The following are points I have in mind for future scaling of this app:

- In the backend I have made the computation of diagnosis to be `async`. This means that in the future if we have an external service which performs the diagnosis we can support this readily within our app.
- The prediction service can use something like AWS Sagemaker or we could use another container service which we can scale separately.
- We start witih a simple monolith which dose the job. In the future we have foresight of splitting parts of our application to different microservices. Note that microservices do not offer a single bullet solution. Since with microservices come more problems including monitoring and ensuring reslience against failures.This is why I always like to start services with a single traditional backend which contains separate modules.

