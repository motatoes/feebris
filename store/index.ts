import { createStore } from 'redux'
import feebrisApp from './reducers'

export type UserType = {
    isAuthenticated: boolean
    username: string,
    email: string,
    token: string
}

export type AssessmentType = {
    temperature: number,
    cough: boolean,
    fever: boolean
}

export type StateType = {
    user: UserType,
    assessment: AssessmentType
}

export const store = createStore(feebrisApp)
