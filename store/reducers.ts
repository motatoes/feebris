import { combineReducers } from 'redux';
import {
  SET_TEMPERATURE,
  SET_COUGH,
  SET_FEVER,
  SET_USER,
  SET_TOKEN,
  LOGOUT,
  SET_RESULT
} from '../constants';

import {
  UserType
} from './index.js'
import { object } from 'prop-types';

const defaultUser = {
  username: undefined,
  email: undefined,
  token: undefined,
  isAuthenticated: false,
}

const defaultAssessment = {
  temperature: 37,
  cough: undefined,
  fever: undefined
}

const defaultResult = {
  status: "Pending"
}

type UserActionType = {
  type: string
  value: any,
}

type AssessmentActionType = {
  type: string,
  value: string | boolean
}

// handles the current user's state
function userReducer (state = defaultUser, action: UserActionType) {
  switch (action.type) {
    case SET_USER:
      return action.value
    case SET_TOKEN:
      return Object.assign({}, state, {
        token: action.value,
        isAuthenticated: true
      })
    case LOGOUT:
      return defaultUser
    default:
      return state
  }
}

// handles the user assessment state
function assessmentReducer(state = defaultAssessment, action: AssessmentActionType) {
  switch (action.type) {
    case SET_TEMPERATURE:
      return Object.assign({}, state, {
        temperature: action.value
      })
    case SET_FEVER:
      return Object.assign({}, state, {
        fever: action.value
      })
    case SET_COUGH:
      return Object.assign({}, state, {
        cough: action.value
      })
    default:
      return state
  }
}

function resultReducer(state = defaultResult, action) {
  switch(action.type) {
    case SET_RESULT:
      return action.result
    default:
      return state
  }
}

const feebrisApp = combineReducers({
  user: userReducer,
  assessment: assessmentReducer,
  result: resultReducer
})

export default feebrisApp
