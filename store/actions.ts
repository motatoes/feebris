import {
  SET_TEMPERATURE,
  SET_COUGH,
  SET_FEVER,
  SET_USER,
  SET_TOKEN,
  LOGOUT,
  SET_RESULT
} from '../constants'

export function setTemprature (value : number) {
  return {
    type: SET_TEMPERATURE,
    value
  }
}

export function setCough (value: boolean) {
  return {
    type: SET_COUGH,
    value
  }
}

export function setFever (value: boolean) {
  return {
    type: SET_FEVER,
    value
  }
}

// set the authenticated user
export function setUser (value: object) {
  return {
    type: SET_USER,
    value
  }
}

export function setToken(value: string) {
  return {
    type: SET_TOKEN,
    value
  }
}

export function logout () {
  return {
    type: LOGOUT
  }
}


// set the diagnosis result
export function setResult (result) {
  return {
    type: SET_RESULT,
    result: result
  }
}
