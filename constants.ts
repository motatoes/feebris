
export const SET_TEMPERATURE = 'SET_TEMPERATURE'
export const SET_FEVER = 'SET_FEVER'
export const SET_COUGH = 'SET_COUGH'
export const SET_USER = 'SET_USER'
export const SET_TOKEN = 'SET_TOKEN'
export const LOGOUT = 'LOGOUT'
export const SET_RESULT = 'SET_RESULT'